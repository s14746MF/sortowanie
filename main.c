#include<stdio.h>
#include<time.h>
#include<stdlib.h>

int Partition(int A[], int p, int r);
int quickSort(int A[], int p, int r);
void heapify(int A[], int n, int i);

int main() {
    int p = 0, i, j = 0, r = 9;
    int A[1000000];

    srand(time(NULL));

    for (i=0; i<=999999;i++){
        A[i]=rand()%500;
    }

    // quickSort
    printf("Starting of the program...\n");
    clock_t begin = clock();
    quickSort(A, p, r);
    clock_t end = clock();
    double diff_t = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Execution time of quickSort = %f\n", diff_t);

    // heapSort
    printf("Starting of the program...\n");
    int n = sizeof(A) / sizeof(A[0]);
    begin = clock();
    heapSort(A, n);
    end = clock();
    diff_t = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Execution time of heapSort = %f\n", diff_t);

    // bubbleSort
    printf("Starting of the program...\n");
    begin = clock();
    bubbleSort(A);
    end = clock();
    diff_t = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Execution time of bubbleSort = %f\n", diff_t);

    return 0;
}

int Partition(int A[], int p, int r) {
    int x = A[r];
    int i = p - 1;
    int j = p;
    for (j; j <= r-1; j++) {
        if (A[j] <= x) {
            i++;
            int pom = A[i];
            A[i] = A[j];
            A[j] = pom;
        }
    }

    int pom2 = A[i+1];
    A[i+1] = A[r];
    A[r] = pom2;

    return i+1;
}

int quickSort(int A[], int p, int r) {

    if (p < r) {
        int q = Partition(A, p, r);
        quickSort(A, p, q - 1);
        quickSort(A, q + 1, r);
    }
}

void heapify(int A[], int n, int i) {
    int largest = i;
    int l = 2 * i + 1;  // left child= 2*i + 1
    int r = 2 * i + 2;  // right child = 2*i + 2

    if (l < n && A[l] > A[largest])
        largest = l;

    if (r < n && A[r] > A[largest])
        largest = r;

    if (largest != i) {
        int pom = A[i];
        A[i] = A[largest];
        A[largest] = pom;

        heapify(A, n, largest);
    }
}

void heapSort(int A[], int n) {

    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(A, n, i);

    for (int i = n - 1; i >= 0; i--) {
        int pom = A[0];
        A[0] = A[i];
        A[i] = pom;

        heapify(A, i, 0);
    }
}

int bubbleSort(int A[]) {

    int n = 999;
    do {
        for (int i = 0; i < n - 1; i++) {
            if (A[i] > A[i + 1]) {
                int pom = A[i];
                A[i] = A[i + 1];
                A[i + 1] = pom;
            }
        }
        n = n - 1;
    } while (n > 1);

    return 0;
}
